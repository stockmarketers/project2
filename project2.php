<html>
	<head>
		<title>Project 2: Login</title>
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<link rel="stylesheet" href="http://code.jquery.com/mobile/1.0.1/jquery.mobile.structure-1.0.1.min.css" />
		<link rel="stylesheet" href="css/text.css" />
		<script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
		<script src="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.js"></script>
	</head>
	<body>
		<div data-role="page" data-theme="a">
			<div class="ui-header ui-bar-a" role="banner" data-role="header" data-form="ui-bar-a" data-theme="a" data-swatch="a">
				<h2 class="head" aria-level="1" role="heading" tabindex="0">Toontown 2012 Elections</h2>
			</div>
			<form method='post' action='checklogin.php'>
				<label for='voterid' class='ui-hidden-accessible'>Voter ID:</label>
				<input name='voterid' type='text' id='voterid' class="input ui-input-text ui-body-null ui-corner-all ui-shadow-inset ui-body-a" data-form="ui-body-a" value='' placeholder="Voter ID" />
				<input type='submit' name='Submit' value='Login' />
			</form>
		</div>
	</body>
</html>