<?php
	
	// Need to get from login page to this one, then from here to some page that says you voted/go to results.
	// Might start something like this, but need to save vote and add it to results.
	$voterid = $_SESSION['voterid'];
	/*
	if(isset($_POST['submit']) && (!$logged_in)) {
		$selectuser_query = "SELECT p.idnumber, p.name FROM people p";
		$user_result = $mysqli->query($user_result);
		if($mysqli->error) {
			print "Query failed: ".$mysqli->error;
		}
		
		while($row = $user_result->fetch_object(MYSQLI_ASSOC)) {
			if ((($_POST['idnumber']) == ($row->idnumber)) && (($_POST['name']) == ($row->name))) {
                $logged_in = true;
                $logged_in_idnumber = $row->idnumber;
            }
        }
    }
    */
?>

<!DOCTYPE html> 
<html> 
	<head> 
		<title>Toontown General Election 2012</title> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<link rel="stylesheet" href="css/vote_theme.min.css" />
		<link rel="stylesheet" href="http://code.jquery.com/mobile/1.0.1/jquery.mobile.structure-1.0.1.min.css" />
		<link rel="stylesheet" href="css/text.css" />
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/mobile/1.0.1/jquery.mobile-1.0.1.min.js"></script>
		<script type="text/javascript" src="js/swipe.js"></script>
	</head> 
	<body> 

		<div data-role="page" id="panel1" data-theme="b">
			<div class="ui-header ui-bar-b" role="banner" data-role="header" data-form="ui-bar-b" data-theme="b" data-swatch="b">
				<h2 class="head" aria-level="1" role="heading" tabindex="0">Toontown Election 2012</h2>
			</div>
			<div class="subhead">
				<p>Welcome, number <? $voterid ?>. Please select a candidate.</p>
			</div><!-- /header -->

			<div class="ui-content ui-body-b" role="main" data-role="content" data-form="ui-body-b" data-theme="b">	
				<h3>Porky Pig</h3>
							<div class="imgcenter">
					<img src="img/porky.jpg" alt="Porky" />
				</div>
				<ul>
					<li>Mild-mannered, but tough on the right issues.</li>
				</ul>
				<form method='post' action='vote.php' data-form="ui-body-b">
					<input name='action' type='submit' value='Vote for Porky' />
				</form>	
			</div><!-- /content -->
			<a href="#panel2" data-role="button" data-form="ui-body-b">Next</a></div>
		</div><!-- /page -->

		<div data-role="page" id="panel2" data-theme="d">
			<div class="ui-header ui-bar-d" role="banner" data-role="header" data-form="ui-bar-d" data-theme="d" data-swatch="d">
				<h2 class="head" aria-level="1" role="heading" tabindex="0">Toontown Election 2012</h2>
			</div>
			<div class="subhead">
				<p>Welcome. Please select a candidate.</p>
			</div><!-- /header -->

			<div class="ui-content ui-body-d" role="main" data-role="content" data-form="ui-body-d" data-theme="d">		
				<h3>Popeye the Sailor Man</h3>	
				<div class="imgcenter">
					<img src="img/popeye.jpg" alt="Popeye" />
				</div>
				<ul>
					<li>Takes on the heavy-hitting issues.</li>
				</ul>
				<form method='post' action='vote.php' data-form="ui-body-d">
					<input name='action' type='submit' value='Vote for Popeye' />
				</form>		
			</div><!-- /content -->
			<div class="ui-grid-a">
				<div class="ui-block-a"><a href="#panel1" data-role="button" data-form="ui-body-d">Previous</a></div>
				<div class="ui-block-b"><a href="#panel3" data-role="button" data-form="ui-body-d">Next</a></div>
			</div>
		</div><!-- /page -->

		<div data-role="page" id="panel3" data-theme="c">
			<div class="ui-header ui-bar-d" role="banner" data-role="header" data-form="ui-bar-d" data-theme="d" data-swatch="d">
				<h2 class="head" aria-level="1" role="heading" tabindex="0">Toontown Election 2012</h2>
			</div>
			<div class="subhead">
				<p>Welcome. Please select a candidate.</p>
			</div><!-- /header -->

			<div class="ui-content ui-body-c" role="main" data-role="content" data-form="ui-body-c" data-theme="c">		
				<h3>Yosemite Sam</h3>
								<div class="imgcenter">
					<img src="img/sambia.gif" alt="Yosemite Sam" />
				</div>	
				<ul>
					<li>A staunch believer in 2nd Amendment rights.</li>
				</ul>
				<form method='post' action='vote.php' data-form="ui-body-c">
					<input name='action' type='submit' value='Vote for Sam' />
				</form>
			</div><!-- /content -->
			<a href="#panel2" data-role="button" data-form="ui-body-c">Previous</a></div>
		</div><!-- /page -->

	</body>
</html>